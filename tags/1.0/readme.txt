=== Post Auto Vertical Scrolling ===
Contributors: shahinurislam
Donate link: https://m.me/md.shahinur.islam.96
Tags: Any Post, custom post, category
Requires at least: 5.4
Tested up to: 6.0
Stable tag: 1.0
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Post Auto Vertical Scrolling plugins bottom to top. Simple but flexible.

== Description ==

Any Post or any custom post category you can view Auto Vertical Scrolling bottom to top as like news tiker. Take full control over your WordPress site, build any shortcode paste you can imagine – no programming knowledge required.

= Docs & Support =

You can find [docs](https://wordpress.org/plugins/post-auto-vertical-scrolling), [FAQ](https://wordpress.org/plugins/post-auto-vertical-scrolling) and more detailed information about Post Auto Vertical Scrolling on [https://m.me/md.shahinur.islam.96](https://m.me/md.shahinur.islam.96). If you were unable to find the answer to your question on the FAQ or in any of the documentation, you should check the [support forum](https://wordpress.org/support/plugin/post-auto-verticall-scrolling/) on WordPress.org. If you can't locate any topics that pertain to your particular issue, post a new topic for it.

= Post Auto Vertical Scrolling Needs Your Support =

It is hard to continue development and support for this free plugin without contributions from users like you. If you enjoy using Post Auto Vertical Scrolling and find it useful, please consider [__making a donation__](https://m.me/md.shahinur.islam.96). Your donation will help encourage and support the plugin's continued development and better user support. Find on gitlab. [Gitlab](https://gitlab.com/shahinurislam/PostAutoVerticalScrolling) 

== Installation ==

1. Upload the entire `Post Auto Vertical Scrolling` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Settings' menu in your WordPress admin panel.

For basic usage, you can also have a look at the [plugin web site](https://wordpress.org/plugins/post-auto-vertical-scrolling).

== Frequently Asked Questions ==

Do you have questions or issues with Post Auto Vertical Scrolling? Use these support channels appropriately.

= Post Auto Vertical Scrolling is responsive? =

Yes this plugin are fully responsive and support every mobile device.

= What about this plugin? =

This plugin is scrolling post or custom post bottom to top.

== Upgrade Notice ==

This is new version 1.0

== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png
5. screenshot-5.png

== Changelog ==

= 1.0 =

* Add new post type.
* Generate shortcode place and show the main file.
